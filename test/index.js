/* eslint-disable no-console */
"use strict";

const nsMatcher = require("..");

const samples = [
  {pattern: "a", matches: ["a"], rejects: ["x", "ax", "xa", "a:", "a:x", ":a"]},
  {pattern: "a:b", matches: ["a:b"], rejects: ["a", "a:b:c"]},
  {pattern: "a:*", matches: ["a:x"], rejects: ["a", "a:x:y"]},
  {pattern: "*:a", matches: ["x:a"], rejects: ["a", "x:y:a"]},
  {pattern: "a:*:b", matches: ["a:x:b"], rejects: ["a:b", "a:x:y:b"]},
  {pattern: "a:**", matches: ["a", "a:x", "a:x:y"], rejects: ["ax", "x:a"]},
  {pattern: "**:a", matches: ["a", "x:a", "x:y:a"], rejects: ["xa", "a:x"]},
  {pattern: "a:**:b", matches: ["a:b", "a:x:b", "a:x:y:b"], rejects: ["ab", "axb"]},
  {pattern: "*,!a:*,a:b", matches: ["a", "x:y", "a:b"], rejects: ["a:x"]}
];

let errors = 0;

for (let sample of samples) {
  const matches = nsMatcher(sample.pattern);
  for (let namespace of sample.matches) {
    if (!matches.test(namespace)) {
      errors++;
      console.error(sample.pattern + " misses " + namespace);
    }
  }
  for (let namespace of sample.rejects) {
    if (matches.test(namespace)) {
      errors++;
      console.error(sample.pattern + " accepts " + namespace);
    }
  }
}

if (errors) {
  console.error("total errors: " + errors);
  process.exit(1);
}
