"use strict";

const DEFAULT_OPTIONS = {
  split: true, // Split passed pattern argument

  matchSoleWildcard: true,
  fullMatch: true,

  patternSeparator: /[,;\s]+/, // String#split argument for patterns
  invertValueChars: "!-", // patterns starting with any of those invert the match value

  // wildcard characters must not be matched by SPECIAL_CHAR_MATCHER
  wildcard: "*",
  wildcardDeep: "**",

  // expressions run on name strings
  scopeSeparatorExp: "[:/]",
  scopeSeparatorNegativeExp: "[^:/]",

  // expressions run on the sanitized pattern strings
  scopeSeparatorPatternExp: "[:/]",
  wildcardPatternExp: "(?:\\*\\*?)",
  wildcardRegex: null, // auto-set within constructor

  specialCharMatcher: /[\\^$+?.()|[\]{}]/g // matcher for characters with special meaning within regex'
};

/*===================================================== Exports  =====================================================*/

module.exports = module.exports.Matcher = Matcher;

/*==================================================== Functions  ====================================================*/

function Matcher(pattern, options) {
  if (this == null) { return new Matcher(pattern, options); }

  // set options
  this.options = options = Object.assign({}, DEFAULT_OPTIONS, options);
  // assign combined wildcardRegex to options if not set
  if (options.wildcardRegex == null) {
    options.wildcardRegex = new RegExp(
        "(^|(?:" + options.scopeSeparatorPatternExp + ")?)(" + options.wildcardPatternExp + ")((?:" + options.scopeSeparatorPatternExp + ")?|$)"
    );
  }

  // private methods
  this._wildcardMatchReplacer = this.wildcardMatchReplacer.bind(this);

  // parse the pattern
  this.parseMany(pattern);
}

Matcher.prototype.test = function test(namespace) {
  const tests = this.tests;
  for (let i = tests.length - 1; i >= 0; i--) {
    const item = tests[i];
    if (item.test(namespace)) { return item.value; }
  }
  return false;
};

// noinspection JSUnusedGlobalSymbols
Matcher.prototype.all = Matcher.prototype.every = function every(namespaces) {
  for (let namespace of namespaces) { if (!this.test(namespace)) { return false; } }
  return true;
};

// noinspection JSUnusedGlobalSymbols
Matcher.prototype.any = Matcher.prototype.some = function some(namespaces) {
  for (let namespace of namespaces) { if (this.test(namespace)) { return true; } }
  return false;
};

Matcher.prototype.parseMany = function (pattern) {
  const options = this.options;
  if (typeof pattern === "string") { pattern = options.split ? pattern.split(options.patternSeparator) : [pattern]; }
  if (!Array.isArray(pattern)) { throw new Error("Expected pattern to be a String or an Array."); }
  const list = [];
  for (let p of pattern) {
    const item = this.parseSingle(p);
    if (item !== null) { list.push(item); }
  }
  this.tests = list;
};

Matcher.prototype.parseSingle = function (patternString) {
  let options = this.options;
  let value = true;
  patternString = patternString.trim();
  let sub = 0;
  while (options.invertValueChars.includes(patternString[sub])) {
    value = !value;
    sub++;
  }
  if (sub !== 0) { patternString = patternString.substring(sub); }
  // ignore empty patterns
  if (!patternString) { return null; }
  // special case "*" and (for improved speed of common usage) "**"
  if (options.matchSoleWildcard && patternString === options.wildcard || patternString === options.wildcardDeep) {
    return {value, test() { return true; }};
  }
  // escape special regular expression characters and replace wildcards with regex equivalent
  const sanitized = patternString
      .replace(options.specialCharMatcher, "\\$&")
      .replace(options.wildcardRegex, this._wildcardMatchReplacer);
  // create regex matcher from string
  const regex = new RegExp(options.fullMatch ? "^" + sanitized + "$" : sanitized);
  return {value, test: regex.test.bind(regex)};
};

Matcher.prototype.wildcardMatchReplacer = function (match, prefix, wildcard, suffix) {
  const options = this.options;
  if (wildcard === options.wildcard) {
    const expand = "(?:" + options.scopeSeparatorNegativeExp + ")*?";
    return (prefix ? options.scopeSeparatorExp : "") + expand + (suffix ? options.scopeSeparatorExp : "");
  } else if (wildcard === options.wildcardDeep) {
    if (prefix) {
      if (suffix) {
        // "$"  "{SEPARATOR}"  "{SEPARATOR}*{SEPARATOR}"
        return "(?:$|" + options.scopeSeparatorExp + "|" + options.scopeSeparatorExp + ".*?" + options.scopeSeparatorExp + ")";
      }
      // "$"  "{SEPARATOR}"  "{SEPARATOR}*"
      return "(?:$|" + options.scopeSeparatorExp + ".*?)";
    }
    if (suffix) {
      // "^"  "{SEPARATOR}"  "*{SEPARATOR}
      return "(?:^|.*?" + options.scopeSeparatorExp + ")";
    }
    // "*"
    return ".*?";
  }
  return match;
};
